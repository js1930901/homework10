/* 
Теоретичні питання
1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?

Для створення нового елементу використовують запис document.createElement ('назва тегу')
Додавати елементи можно:
перед елементом - before
після елемента - after
на початок контенту елемента - prepend
в кінець контенту елемента - append

2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.

- Отримання посилання на елементи з класом "navigation"
  const navigation = document.querySelector('.navigation');

- потім за допомогою методу remove() видалеємо елемент зі сторинки
  navigation.remove()

3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?
after та before

Практичні завдання
 1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.
 
 2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
 Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.
 */

// Задача 1

const link = document.createElement('a')
link.textContent = 'Learn More'
link.href = '#'

const footer = document.querySelector('footer')
footer.append(link)

// const footerChildren = [...footer.children]
// footerChildren.push(link)
// footer.innerHTML = ''
// footer.append(...footerChildren)

// Задача 2

const select = document.createElement('select')
select.id = 'rating'

for (let i = 1; i <= 4; i++) {
  let option = document.createElement('option')
  option.value = i
  if (i === 1) {
    option.textContent = `${i} Star`
  } else {
    option.textContent = `${i} Stars`
  }

  select.append(option)
}

const features = document.querySelector('.features')
features.before(select)
